import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Random;

public class DisplayService {

    public static void writeTxt(String str) {

        Path filePath = Paths.get("D:\\SEM 7\\DS_30644_Giurgi_Iacob\\assignment3\\DVDHistory.txt");
        try {
            if (!Files.exists(filePath)) {
                Files.createFile(filePath);
            }
            StringBuilder stringBuilder = new StringBuilder(str);
            stringBuilder.append(System.lineSeparator());
            Files.write(filePath,stringBuilder.toString().getBytes(), StandardOpenOption.APPEND);

        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
