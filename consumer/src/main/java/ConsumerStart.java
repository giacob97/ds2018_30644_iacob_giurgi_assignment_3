import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class ConsumerStart {


    public static void main(String [] args){
        try {
            sendMessage();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
    }


    public static void sendMessage() throws TimeoutException{
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("localhost");
        Connection connection = null;
        Channel channel = null;
        try {
            connection = connectionFactory.newConnection();
            channel = connection.createChannel();
            channel.exchangeDeclare("dvds","fanout");
            String queueName =  channel.queueDeclare().getQueue();
            channel.queueBind(queueName,"dvds","");

            Consumer consumer = new DefaultConsumer(channel) {
                @Override
                public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
                        throws IOException {
                    String message = new String(body, "UTF-8");
                    MailService mailService = new MailService("javatest97@gmail.com","tehniciprogramare97");
                    mailService.sendMail("giurgi.iacob@gmail.com","DVD nou in magazin",message);
                    mailService.sendMail("giurgi.iacob@gmail.com","DVD nou in magazin",message);
                    DisplayService.writeTxt(message);
                }
            };
            channel.basicConsume(queueName, true, consumer);

        } catch (IOException e) {
            e.printStackTrace();
        }


    }


}
