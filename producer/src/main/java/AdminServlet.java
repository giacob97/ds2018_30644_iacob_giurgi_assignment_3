import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import com.rabbitmq.client.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import java.io.IOException;
import java.sql.Time;
import java.util.concurrent.TimeoutException;

@WebServlet("/admin")
public class AdminServlet extends HttpServlet {


    @Override
    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher rd = request.getRequestDispatcher("WEB-INF/jsp/admin.jsp");
        rd.include(request, response);

    }

    @Override
    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws ServletException,IOException {

         String dvdName = (String) request.getParameter("dvd_name");
         int year = Integer.valueOf((String) request.getParameter("year"));
         double price = Double.valueOf((String) request.getParameter("price"));
         DVD dvd = new DVD(dvdName,year,price);
         try {
           MessageSender.sendMessage(dvd.toString());
         }catch(TimeoutException e){
            e.printStackTrace();
        }

         this.doGet(request,response);
    }



}
