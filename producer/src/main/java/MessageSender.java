import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class MessageSender {


    public static void sendMessage(String message) throws TimeoutException,IOException{
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("localhost");
        Connection connection = connectionFactory.newConnection();
        Channel channel = connection.createChannel();
        channel.exchangeDeclare("dvds", "fanout");
        channel.basicPublish("dvds", "", null, message.getBytes("UTF-8"));
        channel.close();
        connection.close();
    }
}
