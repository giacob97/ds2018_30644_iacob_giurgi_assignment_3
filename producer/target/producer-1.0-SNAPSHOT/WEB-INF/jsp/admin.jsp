<%--
  Created by IntelliJ IDEA.
  User: Iacob
  Date: 10/26/2018
  Time: 5:14 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <style>
        .container {
            width: 200px;
            clear: both;
        }

        .container input {
            width: 100%;
            clear: both;
        }
    </style>
    <title>Admin Page</title>
</head>
<body>
<form action="/admin" method="post">
    <div class="container">
        <h2>Add a dvd</h2>
        Dvd Name: <input type="text" name="dvd_name">
        <br/>
        Year: <input type="text" name="year"/>
        <br/>
        Price: <input type="text" name="price">
        <br/><br/>
        <input type="submit" name="add" value="Submit"/>
    </div>
    <br/>
</form>
</body>
</html>
